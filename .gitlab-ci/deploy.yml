
.deploy image:
  image: $CI_REGISTRY/$CI_PROJECT_PATH/$SG_CI_IMAGE_NAME
  stage: deploy
  tags:
    - stackgres-runner-v2
  variables:
    IMAGE_TAG_SUFFIX: ""
  script:
    - |
      set -e
      [ "$DEBUG" != true ] || set -x
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_SHORT_SHA"}$IMAGE_TAG_SUFFIX"
      export IMAGE_PUSH_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}$IMAGE_TAG_SUFFIX"
      [ -n "$IMAGE_NAME" ]
      export IMAGE_PULL_NAME="$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:$IMAGE_TAG"
      export IMAGE_PUSH_NAME="docker.io/$IMAGE_NAME:$IMAGE_PUSH_TAG"
      ARCH_LIST="amd64 $(
        if [ "$DO_ARM" = true ] \
          || { [ "$SKIP_ARM" != true ] && [ -n "$CI_COMMIT_TAG" ]; }
        then
          echo arm64
        fi
        )"
      for ARCH in $ARCH_LIST
      do
        docker pull "$IMAGE_PULL_NAME-$ARCH"
        docker tag "$IMAGE_PULL_NAME-$ARCH" "$IMAGE_PUSH_NAME-$ARCH"
      done
      if docker manifest inspect "$IMAGE_PUSH_NAME" >/dev/null 2>&1
      then
        docker pull registry.gitlab.com/ongresinc/stackgres/placeholder
        docker tag registry.gitlab.com/ongresinc/stackgres/placeholder "$IMAGE_PUSH_NAME"
      fi
      mv ~/.docker/config.json ~/.docker/gitlab.json
      mv "$REGISTRY_AUTH_FILE" ~/.docker/config.json
      for ARCH in $ARCH_LIST
      do
        docker push "$IMAGE_PUSH_NAME-$ARCH"
      done
      if docker manifest inspect "$IMAGE_PUSH_NAME" >/dev/null 2>&1
      then
        docker push "$IMAGE_PUSH_NAME"
      fi
      docker manifest rm "$IMAGE_PUSH_NAME" 2>/dev/null || true
      docker manifest create "$IMAGE_PUSH_NAME" $(
        for ARCH in $ARCH_LIST
        do
          echo "$IMAGE_PUSH_NAME-$ARCH "
        done)
      docker manifest push "$IMAGE_PUSH_NAME"
  retry: 2
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/
      - $DO_DEPLOY

deploy operator jvm image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: "-jvm"
    IMAGE_NAME: "stackgres/operator"

deploy restapi jvm image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: "-jvm"
    IMAGE_NAME: "stackgres/restapi"

deploy jobs jvm image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: "-jvm"
    IMAGE_NAME: "stackgres/jobs"

deploy cluster-controller jvm image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: "-jvm"
    IMAGE_NAME: "stackgres/cluster-controller"

deploy distributedlogs-controller jvm image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: "-jvm"
    IMAGE_NAME: "stackgres/distributedlogs-controller"

deploy operator native image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/operator"
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/

deploy restapi native image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/restapi"
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/

deploy job native image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/jobs"
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/

deploy adminui image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/admin-ui"

deploy cluster-controller native image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/cluster-controller"
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/

deploy distributedlogs-controller native image:
  extends: .deploy image
  variables:
    IMAGE_TAG_SUFFIX: ""
    IMAGE_NAME: "stackgres/distributedlogs-controller"
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $SKIP_NATIVE != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/

deploy helm packages and templates:
  image: $CI_REGISTRY/$CI_PROJECT_PATH/$SG_CI_IMAGE_NAME
  stage: deploy
  script:
    - |
      set -e
      STACKGRES_VERSION=$(grep '<artifactId>stackgres-parent</artifactId>' "stackgres-k8s/src/pom.xml" -A 2 -B 2 \
        | grep -o '<version>\([^<]\+\)</version>' | tr '<>' '  ' | cut -d ' ' -f 3)
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      sh stackgres-k8s/ci/build/build-gitlab.sh extract helm-packages stackgres-k8s/install/helm/target/packages
      sh stackgres-k8s/ci/build/build-gitlab.sh extract helm-templates stackgres-k8s/install/helm/target/templates
      mkdir -p "stackgres-k8s/install/helm/target/public/downloads/stackgres-k8s/stackgres"
      cp -a stackgres-k8s/install/helm/target/templates \
        "stackgres-k8s/install/helm/target/public/downloads/stackgres-k8s/stackgres/$STACKGRES_VERSION"
      cp -a stackgres-k8s/install/helm/target/packages \
        "stackgres-k8s/install/helm/target/public/downloads/stackgres-k8s/stackgres/$STACKGRES_VERSION/helm"
      aws s3 sync ./stackgres-k8s/install/helm/target/public/ s3://$S3_BUCKET_NAME/
      aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION_ID --paths '/*'
  cache:
    - key: hashes
      paths:
      - stackgres-k8s/ci/build/target/*
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/
      - $DO_DEPLOY

pages:
  stage: deploy
  image: $CI_REGISTRY/$CI_PROJECT_PATH/$SG_CI_IMAGE_NAME
  environment:
    name: development
    url: https://ongresinc.gitlab.io/stackgres/
  script:
    - sh stackgres-k8s/ci/build/build-gitlab.sh extract documentation doc/public
    - tar cC doc public | tar x
    - find public | sed -e "s/[^-][^\/]*\// |/g" -e "s/|\([^ ]\)/|-\1/"
  retry: 2
  cache:
    - key: hashes
      paths:
      - stackgres-k8s/ci/build/target/*
  artifacts:
    paths:
    - public
  only:
    variables:
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_REF_NAME =~ /^main.*$/
      - $SKIP_DEPLOY != "true" && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/
      - $DO_PAGES
